$(function() {

    var wallpaperNumber = 3;

    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    $('html').addClass("v" + Math.round(getRandomArbitrary(0, wallpaperNumber)));

    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            $img.wrap('<div class="svg-container"></div>');
            if ($img.hasClass('vertical')) {
                $img.parent().addClass('vertical');
            } else {
                $img.parent().addClass('horizontal');
            }

            // Replace image with new SVG
            $clone = $svg.clone();
            $img.after($svg).after($clone);
        }, 'xml');

    });

});